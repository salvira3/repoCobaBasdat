from django.db import models


class Polling(models.Model):
    id = models.IntegerField(primary_key=True)
    polling_start = models.DateTimeField()
    polling_end = models.DateTimeField()
    total_responden = models.IntegerField(default = 0)

    class Meta:
        managed = False
        db_table = 'polling'


class PollingBerita(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url_berita = models.ForeignKey('apps_public.Berita', models.DO_NOTHING, db_column='url_berita')
       

    class Meta:
        managed = False
        db_table = 'polling_berita'


class PollingBiasa(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'polling_biasa'

