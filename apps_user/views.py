from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.db import connection
from apps_login.models import *
from apps_public.models import *


# Create your views here.
response={}

def edit_profile(request):
    response['edit'] = "Disini halaman edit profil user"
    return render(request, 'edit_profile.html', response)

def show_profile(request):
	cursor = connection.cursor()
	cursor.execute('select count(*) from narasumber')
	ncount = cursor.fetchone()[0]
	if ncount != 0:
		nara = request.session['id']
		query = 'select nama from narasumber where id='+str(nara)
		cursor.execute(query)
		nama = cursor.fetchone()[0]
		response['nama'] = nama

		query = 'select username from narasumber where id='+str(nara)
		cursor.execute(query)
		username = cursor.fetchone()[0]
		response['username'] = username

		query1 = 'select id_narasumber from mahasiswa where id_narasumber='+str(nara)
		query2 = 'select id_narasumber from dosen where id_narasumber='+str(nara)
		query3 = 'select id_narasumber from staf where id_narasumber='+str(nara)

		role = ''
		cursor.execute(query1)
		q = cursor.fetchone()
		if(q is not None):
			role = 'Mahasiswa'
			response['no_text'] = 'NPM'
			query = 'select npm from mahasiswa where id_narasumber='+str(nara)
			cursor.execute(query)
			number = cursor.fetchone()[0]
			print (number)
			response['number'] = number


		cursor.execute(query2)
		q = cursor.fetchone()
		if(q is not None):
			role = 'Dosen'
			response['no_text'] = 'NIK'
			query = 'select nik_dosen from dosen where id_narasumber='+str(nara)
			cursor.execute(query)
			number = cursor.fetchone()[0]
			print (number) 
			response['number'] = number

			query = 'select jurusan from dosen where id_narasumber='+str(nara)
			cursor.execute(query)
			jurusan = cursor.fetchone()[0]
			print (jurusan)
			request.session['role'] = 'dosen'
			response['jurusan'] = jurusan

		cursor.execute(query3)
		q = cursor.fetchone()
		if(q is not None):
			role = 'Staff'
			response['no_text'] = 'NIK'
			query = 'select nik_staf from staf where id_narasumber='+str(nara)
			cursor.execute(query)
			number = cursor.fetchone()[0]
			print (number)
			response['number'] = number

			query = 'select posisi from staf where id_narasumber='+str(nara)
			cursor.execute(query)
			posisi = cursor.fetchone()[0]
			print (posisi)
			request.session['role'] = 'staf'
			response['posisi'] = posisi

		response['role'] = role

		query = 'select email from narasumber where id='+str(nara)
		cursor.execute(query)
		email = cursor.fetchone()[0]
		response['email'] = email

		query = 'select no_hp from narasumber where id='+str(nara)
		cursor.execute(query)
		phone = cursor.fetchone()[0]
		response['phone'] = phone

		query = 'select tempat from narasumber where id='+str(nara)
		cursor.execute(query)
		place = cursor.fetchone()[0]
		response['place'] = place

		query = "select to_char(tanggal, "+"'"+"DD Month YYYY"+"'"+") as newtang from narasumber where id="+str(nara)
		cursor.execute(query)
		birthdate = cursor.fetchone()[0]
		response['birthdate'] = birthdate

		query = 'select id_berita, judul, url, created_at, updated_at, topik, jumlah_kata from berita, narasumber_berita, narasumber where id_narasumber = id and url_berita = url and id='+str(nara)
		cursor.execute(query)
		berita = cursor.fetchall()
		print(berita)
		#news = []
		#for x in berita:
		#	tmp = Berita.objects.get(url=x[0])
		#	news.append(tmp)
		
		response['news'] = berita



		return render(request, 'profile_page.html', response)
