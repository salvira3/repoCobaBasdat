from django.db import models
from apps_login.models import *

from datetime import datetime

# Create your models here.

class Berita(models.Model):
    url = models.CharField(primary_key=True, max_length=50)
    judul = models.CharField(max_length=100)
    topik = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)
    jumlah_kata = models.IntegerField()
    rerata_rating = models.FloatField(default = 5)
    id_universitas = models.ForeignKey('apps_login.Universitas', models.DO_NOTHING, db_column='id_universitas')
    id_berita = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'berita'

class NarasumberBerita(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber')

    class Meta:
        managed = False
        db_table = 'narasumber_berita'
        unique_together = (('url_berita', 'id_narasumber'),)


class Tag(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    tag = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'tag'
        unique_together = (('url_berita', 'tag'),)