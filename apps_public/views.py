from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .form import *
from django.db import connection
from django.contrib import messages
from datetime import datetime

# Create your views here.
response={}

def news_page(request):
    response['news_page'] = "Disini halaman home/news"
    response ['form_berita'] = BeritaForm
    news = Berita.objects.all()
    response['news'] = news

    return render(request, 'news_page.html', response)

def news_detail(request):
    response['detail_news'] = "Disini halaman detail news berisi komentar dll"
    return render(request, 'news_detail.html', response)

@csrf_exempt
def createNews(request):
    form = BeritaForm(data=request.POST)
    cursor = connection.cursor()

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()
        query = ('select count(*) from Berita')
        cursor.execute(query)
        count = cursor.fetchone()[0] #(2,)
      
        id_berita = 0
        if count > 0:
            id_berita = count + 1
        else:
            id_berita = 1

        Judul = request.POST.get('Judul', False)
        Url = "http://bmnc_news.com/news-" + str(id_berita)
        Topik = request.POST.get('Topik', False)
        JumlahKata= request.POST.get('JumlahKata', False)
        Tag = request.POST.get('Tag', False)
        narasumber_id = request.session['id']
        waktu = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        query = 'select id_universitas from narasumber where id = ' + str(narasumber_id) 
        count = cursor.execute (query)
        id_univ = cursor.fetchone()[0]
        
        query = ("insert into berita (url, judul, topik, jumlah_kata, id_universitas, id_berita, created_at, updated_at, rerata_rating) values ('" 
            + str(Url) + "','" + str(Judul) + "','" +str(Topik) +"'," + str(JumlahKata) + "," + str(id_univ) +"," + str(id_berita) + ",'" 
            + str(waktu)+ "','" + str(waktu) +"'," + str(0) + ");")
        cursor.execute(query)

        tagg = Tag.replace(" ", "")
        tags = tagg.split(",")
        for i in range (0,len(tags)):
            print (str(tags[i]))
            query = "insert into tag (url_berita, tag) values('" + str(Url) + "','" + str(tags[i]) + "');"
            cursor.execute(query)
        
        query = "insert into Narasumber_Berita (url_berita, id_narasumber) values ('" + str(Url) + "'," + str(narasumber_id) + ");"
        cursor.execute(query)

        messages.success(request, "News Posted!")
        return HttpResponseRedirect(reverse('apps-public:news-page'))
    else :
    	return HttpResponse("null")



