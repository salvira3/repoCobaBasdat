from django.apps import AppConfig


class AppsPublicConfig(AppConfig):
    name = 'apps_public'
