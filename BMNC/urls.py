"""BMNC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import apps_user.urls as apps_user
import apps_login.urls as apps_login
import apps_public.urls as apps_public
import apps_polling.urls as apps_polling

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(apps_login, namespace='apps-login')),
    url(r'^news/', include(apps_public, namespace='apps-public')),
    url(r'^polling/', include(apps_polling, namespace='apps-polling')),
    url(r'^user/', include(apps_user, namespace='apps-user')),
    url(r'^$', RedirectView.as_view(permanent=True, url='news/'), name='index'),
]
